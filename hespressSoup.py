#______________________________________________ Libraires _______________________________________
from sklearn.feature_extraction.text import CountVectorizer  
from nltk.tokenize.treebank import TreebankWordDetokenizer
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from bidi.algorithm import get_display
from gensim import corpora, models
from nltk.corpus import stopwords
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
from datetime import date
import arabic_reshaper
import pandas as pd
import numpy as np
import collections
import requests
import re

#______________________________________________ stop words arabic  _____________________________
stop_words = set(stopwords.words('arabic'))

#______________________________________________ Gather links of all pages  _______________ _____
def getLinks(link):
    tabHttps=[]
    tabHtml=[]
    
    page = requests.get(link)
    #get a code html of the page
    soup = BeautifulSoup(page.content, 'html.parser')
    
    head = soup.find('div',id="container")
    # get all links of the page 
    liens = head.find_all('a',href=True)
    
    for i in liens:
        result = re.split(r"\.", i['href'])
        if 'html' in result:
            result2 = re.split(r"\:", result[0])
            if 'https' in result2:
              tabHttps.append(i['href']) 
            else:
                tabHtml.append(i['href'])
    return [tabHttps,tabHtml]

def getLinksTotal(link):
    tab = []
    tabHtmlGolbal = []
    
    # get links 
    data = getLinks(link)
    
    # get once link of each links
    tabHttps = list(set(data[0]))
    html = list(set(data[1]))
    
    for i in range(len(html)):
        tabHtmlGolbal.append(html[i])
    
    #### extract links of the list tabHttps contain comments
    for i in range(len(tabHttps)):
        data2 = getLinks(tabHttps[i])
        
        # extract just links of the archive and index because author links don't contain comments
        https=list(set(data2[0]))
        
        for k in range(len(https)):
            
            result = re.split(r"\/", https[k])
            for j in range(len(result)):
                string = result[j]
                answer = re.split("\.", string)
                if 'index' in answer or 'archives' in answer:
                    tab.append(https[k])
                
    #table of links archive and index             
    tab = list(set(tab))
    
    for i in range(len(tab)):
        data3 = getLinks(tab[i])
        html = list(set(data3[1]))
        for j in range(len(html)):
            tabHtmlGolbal.append(html[j])
            
    # return all links which contains comments    
    tabHtmlGolbal=list(set(tabHtmlGolbal)) 
    return tabHtmlGolbal
     
#______________________________________________ Gather comments of each page ______________________            
def readComments(tab):
    text=" "
    for i in range(len(tab)):
        result = re.split(r"\:",tab[i])
        
        if 'https' in result:
            page = requests.get(tab[i])
        else:
            # get a page
            page = requests.get("https://www.hespress.com/"+tab[i])
        
        # content of a page
        soup = BeautifulSoup(page.content, 'html.parser')
        try:
            # find a div of all comments
            comments = soup.find('div', id="comment_list")
            # a body of comments
            listComment=comments.find_all('div',class_="comment_body")
             
            # get a date of all comments
            for i in range(len(listComment)):
                date = listComment[i].find('span',class_="comment_date")
                # get date text
                date = date.text.strip()
                
                # split by whitespace
                date = re.split(r"\ ",date)
                if confirmDate(date):
                    #remove a code html
                    comment = re.sub("<.*?>",' ',str(listComment[i].find('div',class_="comment_text")))
                    text=text+' '+comment
        except:
            continue
    return text;

def confirmDate(date1):
    # dictionnary of arabic months 
    arrayMoonths={'يناير':1,'فبراير':2,'مارس':3,'ابريل':4 ,'ماي':5,'يونيو':6,'يوليوز':7,'غشث':8,'شتنبر':9,'أكتوبر':10,'نونبر':11,'دجنبر':12}
    
    # date of system
    today = date.today()
    # format of date
    d1 = today.strftime("%d/%m/%Y")
    
    # split string date of the system
    date2 = re.split(r"/",d1)
    
    # take a month of the post
    month1 = int(arrayMoonths[date1[2]])
    # take a year of the post
    year1 = int(date1[3])
    
    # take a month of the system
    month2 = int(date2[1])
    # take a year of the system
    year2=int(date2[2])
    # if we are in the same year
    if year2-year1 == 0:
        if month2-month1 <= 2:
            return True;
        else:
            return False;
    # if we are in the new year
    if year2-year1==1:
        if month1-month2>=10:
            return True;
        else:
            return False;
    else:
        return False;
    
#______________________________________________ Preprocessing ___________________________________   
def preprocessing(text):

    # make all text lower case
    text = text.lower()
    # we save just text so remove all punctutions
    toke = RegexpTokenizer(r'\w+')
    tokenizes = toke.tokenize(text)
    #remove stopwords
    text = [word for word in tokenizes if word not in stop_words]
    #remove numbers
    text = [word for word in text  if re.search('[1-9A-Za-z]',word) == None]
    #lemmatization
    lemmatizer = WordNetLemmatizer()
    text = [lemmatizer.lemmatize(word) for word in text]

    #untokenize
    #data TreebankWordDetokenizer().detokenize(text)
         
    r= pd.DataFrame({'data':text})
    r.to_csv("dataComments.csv",index=False,encoding='utf-8-sig')
   
#______________________________________________ convert DataFrame to String   ______________________  
def array2string(array):
    # convert DataFrame to string 
    string=' '
    # dictionnary of the number of times each word in string
    wordcount = {}
    for index, row in array.iterrows():
        # row['data'] is a word
        string=string+' '+row['data']
        if row['data'] not in wordcount:
            wordcount[row['data']] = 1
        else:
            wordcount[row['data']] += 1
    return [string ,wordcount]   

#______________________________________________ display 5 most commun words   ___________________ 
def most_commun_words(string , wordcount):
    # Print number of most common words do you want
    nb_word = int(input("How many most common words do you want : "))
    
    wordcounter = collections.Counter(wordcount)
    for word, count in wordcounter.most_common(nb_word):
        print(word, ": ", count)
        
    # Create a data frame of the most common words 
    # Draw a bar chart
    words = wordcounter.most_common(nb_word)
    
    indexes = []
    
    # do a reshapng for plotting correct arabic words as indexes
    for term in words:
        terme = arabic_reshaper.reshape(term[0])
        indexes.append((get_display(terme),term[1]))
        
    # create a DataFrame
    df = pd.DataFrame(indexes, columns = ['Word', 'Count'])
    
    # display a graph
    df.plot.bar(x='Word',y='Count')
    
#______________________________________________ Could of words _____________________________________    
def show_wordcloud(data,title):
    # for displing the same words in could
    data = arabic_reshaper.reshape(data)
    data = get_display(data)
    
    # create a cloud 
    wordCloud = WordCloud(font_path='arial', background_color='white',mode='RGB', width=2000, height=1000).generate(data)
    plt.title(title)
    plt.imshow(wordCloud)
    plt.axis("off")
    plt.show()
    
#______________________________________________ main  _____________________________________________ 
if __name__=='__main__':
    
    #tab = getLinksTotal('https://www.hespress.com')
    #text = readComments(tab)
    #preprocessing(text)
    data = pd.read_csv('dataComments.csv')
    [string , wordcount] = array2string(data)
    #most_commun_words(string , wordcount)
    #show_wordcloud(string,'Comments')
    
    
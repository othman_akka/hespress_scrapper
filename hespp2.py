#______________________________________________ Libraires _______________________________________
from bidi.algorithm import get_display
from nltk.corpus import stopwords
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
from datetime import date
import arabic_reshaper
import pandas as pd
import requests
import nltk
import re
nltk.download()

stop_words = set(stopwords.words('arabic'))

#______________________________________________ Gather links of all pages  ______________________
def getLinks(link):
    tabHttps=[]
    tabHtml=[]
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'html.parser')
    head = soup.find('div',id="container")
    liens=head.find_all('a',href=True)
    for i in liens:
        result = re.split(r"\.", i['href'])
        if 'html' in result:
            result2 = re.split(r"\:", result[0])
            if 'https' in result2:
              tabHttps.append(i['href']) 
            else:
                tabHtml.append(i['href'])
    return [tabHttps,tabHtml]

def getLinksTotal(link):
    tab=[]
    tabHtmlGolbal=[]
    data=getLinks(link)
    tabHttps=list(set(data[0]))
    html=list(set(data[1]))
    for i in range(len(html)):
        tabHtmlGolbal.append(html[i])
    
    for i in range(len(tabHttps)):
        data2=getLinks(tabHttps[i])
        #on va extraire seulement les liens archives et index car les autres ne contient pas des commentaires ou les autres liens
        https=list(set(data2[0]))
        for k in range(len(https)):
            result = re.split(r"\/", https[k])
            for j in range(len(result)):
                string=result[j]
                answer=re.split("\.", string)
                if 'index' in answer or 'archives' in answer:
                    tab.append(https[k])
                
    #table de liens archives et les index              
    tab=list(set(tab))
    for i in range(len(tab)):
        data3=getLinks(tab[i])
        html=list(set(data3[1]))
        for j in range(len(html)):
            tabHtmlGolbal.append(html[j])
        
    tabHtmlGolbal=list(set(tabHtmlGolbal)) 
    return tabHtmlGolbal
    
    
#______________________________________________ Gather comments of each page ______________________            
def readComments(tab):
    text=" "
    for i in range(len(tab)):
        result = re.split(r"\:",tab[i])
        
        if 'https' in result:
            page = requests.get(tab[i])
        else:
            
            page = requests.get("https://www.hespress.com/"+tab[i])
            
        soup = BeautifulSoup(page.content, 'html.parser')
        try:
            comments = soup.find('div', id="comment_list")
            listComment=comments.find_all('div',class_="comment_body")
                
            for i in range(len(listComment)):
                date=listComment[i].find('span',class_="comment_date")
                date=date.text.strip()
                date= re.split(r"\ ",date)
                if confirmDate(date):
                    comment=re.sub("<.*?>",' ',str(listComment[i].find('div',class_="comment_text")))
                    text=text+' '+comment
                    #print(text)
                    #print(date)
        except:
            continue
    return text;

def confirmDate(date1):
    arrayMoonths={'يناير':1,'فبراير':2,'مارس':3,'ابريل':4 ,'ماي':5,'يونيو':6,'يوليوز':7,'غشث':8,'شتنبر':9,'أكتوبر':10,'نونبر':11,'دجنبر':12}
   
    today = date.today()
    d1 = today.strftime("%d/%m/%Y")
    date2=re.split(r"/",d1)
    
    month1=int(arrayMoonths[date1[2]])
    year1=int(date1[3])
    
    month2=int(date2[1])
    year2=int(date2[2])
    
    if year2-year1==0:
        if month2-month1<=2:
            return True;
        else:
            return False;
    if year2-year1==1:
        if month1-month2>=10:
            return True;
        else:
            return False;
    else:
        return False;
    
#______________________________________________ Preprocessing _____________________________________   
def removeStopWords(text):
    text1=' '
    dataClean=[]
    for word in re.findall(r'[\u0600-\u06FF]+', text):
        text1=text1+' '+word
    for word in text1.split():
        if word not in stop_words:
            dataClean.append(word)
            
    r= pd.DataFrame({'data':dataClean})
    r.to_csv("dataComments.csv",index=False,encoding='utf-8-sig')
 
def arrayToString(array):
    string=' '
    for index, row in array.iterrows():
        string=string+' '+row['data']
    return string           

#______________________________________________ Could  ___________________________________________    
def show_wordcloud(data,title):
    data = arabic_reshaper.reshape(data)
    data = get_display(data) 
    wordCloud = WordCloud(font_path='arial', background_color='white',mode='RGB', width=2000, height=1000).generate(data)
    plt.title(title)
    plt.imshow(wordCloud)
    plt.axis("off")
    plt.show()
   
#______________________________________________ main  _____________________________________________ 
if __name__=='__main__':
    tab=[]
    text1=" "
    #tab=getLinksTotal('https://www.hespress.com')
    #text=readComments(tab)
    #removeStopWords(text)
    data=pd.read_csv('dataComments.csv')
    string=arrayToString(data)
    show_wordcloud(string,'Comments')
    

